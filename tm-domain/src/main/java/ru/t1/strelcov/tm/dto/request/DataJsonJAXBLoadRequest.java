package ru.t1.strelcov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonJAXBLoadRequest extends AbstractUserRequest {

    public DataJsonJAXBLoadRequest(@Nullable final String token) {
        super(token);
    }

}
