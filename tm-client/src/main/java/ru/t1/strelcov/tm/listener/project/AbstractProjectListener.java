package ru.t1.strelcov.tm.listener.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.listener.AbstractListener;

public abstract class AbstractProjectListener extends AbstractListener {

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("[Id]: " + project.getId());
        System.out.println("[Name]: " + project.getName());
        System.out.println("[Description]: " + project.getDescription());
        System.out.println("[Status]: " + project.getStatus().getDisplayName());
        System.out.println("[User Id]: " + project.getUserId());
    }

}
