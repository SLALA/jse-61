package ru.t1.strelcov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;

@Component
public final class UserShowProfileListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Display the current user profile data.";
    }

    @Override
    @EventListener(condition = "@userShowProfileListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[PROFILE]");
        @NotNull final UserDTO user = authEndpoint.getProfile(new UserProfileRequest(getToken())).getUser();
        showUser(user);
    }

}
