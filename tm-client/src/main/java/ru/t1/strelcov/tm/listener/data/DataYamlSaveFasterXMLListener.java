package ru.t1.strelcov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.DataYamlFasterXMLSaveRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.event.ConsoleEvent;

@Component
public final class DataYamlSaveFasterXMLListener extends AbstractDataListener {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-yaml-save-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to yaml file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlSaveFasterXMLListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA YAML SAVE]");
        dataEndpoint.saveYamlFasterXMLData(new DataYamlFasterXMLSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
