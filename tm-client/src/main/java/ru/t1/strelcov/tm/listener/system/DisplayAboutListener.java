package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

@Component
public final class DisplayAboutListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    @EventListener(condition = "@displayAboutListener.name() == #event.name || @displayAboutListener.arg() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println(propertyService.getName());
        System.out.println(propertyService.getEmail());
    }

}
