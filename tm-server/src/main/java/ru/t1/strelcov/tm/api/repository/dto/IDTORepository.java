package ru.t1.strelcov.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.strelcov.tm.dto.model.AbstractEntityDTO;

@NoRepositoryBean
public interface IDTORepository<E extends AbstractEntityDTO> extends JpaRepository<E, String> {

}
